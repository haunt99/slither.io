'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Created by wanghx on 4/27/16.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * 视窗类 由于视窗在整个游戏中只有一个, 所以做成单例
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */

var _map = __webpack_require__(5);

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// 视窗类

var Frame = function () {
  function Frame() {
    _classCallCheck(this, Frame);
  }

  _createClass(Frame, [{
    key: 'init',
    value: function init(options) {
      var self = this;

      this.x = options.x;
      this.y = options.y;
      this.width = options.width;
      this.height = options.height;

      _map2.default.on('scale_changed', function () {
        self.x = _map2.default.relative(self.x);
        self.y = _map2.default.relative(self.y);
      });
    }

    /**
     * 跟踪某个对象
     */

  }, {
    key: 'track',
    value: function track(obj) {
      this.translate(_map2.default.relative(obj.x) - this.x - this.width / 2, _map2.default.relative(obj.y) - this.y - this.height / 2);
    }

    /**
     * 移动视窗
     * @param x
     * @param y
     */

  }, {
    key: 'translate',
    value: function translate(x, y) {
      this.x += x;
      this.y += y;
    }
  }]);

  return Frame;
}();

exports.default = new Frame();

/*****************
 ** WEBPACK FOOTER
 ** ./src/lib/frame.js
 ** module id = 6
 ** module chunks = 0
 **/