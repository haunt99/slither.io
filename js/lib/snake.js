'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _base = __webpack_require__(4);

var _base2 = _interopRequireDefault(_base);

var _map = __webpack_require__(5);

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by wanghx on 4/23/16.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * snake
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var SPEED = 3;
var BASE_ANGLE = Math.PI * 200; // 用于保证蛇的角度一直都是正数

// 蛇头和蛇身的基类

var SnakeBase = function (_Base) {
  _inherits(SnakeBase, _Base);

  function SnakeBase(options) {
    _classCallCheck(this, SnakeBase);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(SnakeBase).call(this, options));

    var self = _this;

    // 皮肤颜色
    _this.color = options.color;
    // 描边颜色
    _this.color_2 = '#000';

    // 垂直和水平速度
    _this.vx = SPEED;
    _this.vy = 0;

    // 生成元素图片镜像
    _this.createImage();

    // 监听地图scale更改事件, 重绘镜像
    _map2.default.on('scale_changed', function () {
      self.createImage();
    });
    return _this;
  }

  // 设置基类的速度


  _createClass(SnakeBase, [{
    key: 'setSize',


    /**
     * 设置宽度和高度
     * @param width
     * @param height
     */
    value: function setSize(width, height) {
      this.width = width;
      this.height = height || width;
      this.createImage();
    }

    /**
     * 生成图片镜像
     */

  }, {
    key: 'createImage',
    value: function createImage() {
      this.img = this.img || document.createElement('canvas');
      this.img.width = this.paintWidth + 10;
      this.img.height = this.paintHeight + 10;
      this.imgctx = this.img.getContext('2d');

      this.imgctx.lineWidth = 2;
      this.imgctx.save();
      this.imgctx.beginPath();
      this.imgctx.arc(this.img.width / 2, this.img.height / 2, this.paintWidth / 2, 0, Math.PI * 2);
      this.imgctx.fillStyle = this.color;
      this.imgctx.strokeStyle = this.color_2;
      this.imgctx.stroke();
      this.imgctx.fill();
      this.imgctx.restore();
    }

    /**
     * 更新位置
     */

  }, {
    key: 'update',
    value: function update() {
      this.x += this.vx;
      this.y += this.vy;
    }

    /**
     * 渲染镜像图片
     */

  }, {
    key: 'render',
    value: function render() {
      this.update();

      // 如果该元素在视窗内不可见, 则不进行绘制
      if (!this.visible) return;

      // 如果该对象有角度属性, 则使用translate来绘制, 因为要旋转
      if (this.hasOwnProperty('angle')) {
        _map2.default.ctx.save();
        _map2.default.ctx.translate(this.paintX, this.paintY);
        _map2.default.ctx.rotate(this.angle - BASE_ANGLE - Math.PI / 2);
        _map2.default.ctx.drawImage(this.img, -this.img.width / 2, -this.img.height / 2);
        _map2.default.ctx.restore();
      } else {
        _map2.default.ctx.drawImage(this.img, this.paintX - this.img.width / 2, this.paintY - this.img.height / 2);
      }

      // 绘制移动方向, debug用
      //    map.ctx.beginPath();
      //    map.ctx.moveTo(
      //      this.paintX - (this.paintWidth * 0.5 * this.vx / this.speed),
      //      this.paintY - (this.paintWidth * 0.5 * this.vy / this.speed)
      //    );
      //    map.ctx.lineTo(this.paintX, this.paintY);
      //    map.ctx.strokeStyle = '#000';
      //    map.ctx.stroke();
    }
  }, {
    key: 'speed',
    set: function set(val) {
      this._speed = val;

      // 重新计算水平垂直速度
      this.velocity();
    },
    get: function get() {
      return this._speed ? this._speed : this._speed = this.tracer ? this.tracer.speed : SPEED;
    }
  }]);

  return SnakeBase;
}(_base2.default);

// 蛇的身躯类


var SnakeBody = function (_SnakeBase) {
  _inherits(SnakeBody, _SnakeBase);

  function SnakeBody(options) {
    _classCallCheck(this, SnakeBody);

    // 设置跟踪者

    var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(SnakeBody).call(this, options));

    _this2.tracer = options.tracer;

    _this2.tracerDis = _this2.distance;

    _this2.savex = _this2.tox = _this2.tracer.x - _this2.distance;
    _this2.savey = _this2.toy = _this2.tracer.y;
    return _this2;
  }

  _createClass(SnakeBody, [{
    key: 'update',
    value: function update() {
      if (this.tracerDis >= this.distance) {
        var tracer = this.tracer;

        // 计算位置的偏移量
        this.tox = this.savex + (this.tracerDis - this.distance) * tracer.vx / tracer.speed;
        this.toy = this.savey + (this.tracerDis - this.distance) * tracer.vy / tracer.speed;

        this.velocity(this.tox, this.toy);

        this.tracerDis = 0;

        // 保存tracer位置
        this.savex = this.tracer.x;
        this.savey = this.tracer.y;
      }

      this.tracerDis += this.tracer.speed;

      if (Math.abs(this.tox - this.x) <= Math.abs(this.vx)) {
        this.x = this.tox;
      } else {
        this.x += this.vx;
      }

      if (Math.abs(this.toy - this.y) <= Math.abs(this.vy)) {
        this.y = this.toy;
      } else {
        this.y += this.vy;
      }
    }

    /**
     * 根据目标点, 计算速度
     * @param x
     * @param y
     */

  }, {
    key: 'velocity',
    value: function velocity(x, y) {
      this.tox = x || this.tox;
      this.toy = y || this.toy;

      var disX = this.tox - this.x;
      var disY = this.toy - this.y;
      var dis = Math.hypot(disX, disY);

      this.vx = this.speed * disX / dis || 0;
      this.vy = this.speed * disY / dis || 0;
    }
  }, {
    key: 'distance',
    get: function get() {
      return this.tracer.width * 0.2;
    }
  }]);

  return SnakeBody;
}(SnakeBase);

// 蛇头类


var SnakeHeader = function (_SnakeBase2) {
  _inherits(SnakeHeader, _SnakeBase2);

  function SnakeHeader(options) {
    _classCallCheck(this, SnakeHeader);

    var _this3 = _possibleConstructorReturn(this, Object.getPrototypeOf(SnakeHeader).call(this, options));

    _this3.angle = BASE_ANGLE + Math.PI / 2;
    _this3.toAngle = _this3.angle;
    return _this3;
  }

  /**
   * 添加画眼睛的功能
   */


  _createClass(SnakeHeader, [{
    key: 'createImage',
    value: function createImage() {
      _get(Object.getPrototypeOf(SnakeHeader.prototype), 'createImage', this).call(this);
      var self = this;
      var eyeRadius = this.paintWidth * 0.2;

      function drawEye(eyeX, eyeY) {
        self.imgctx.beginPath();
        self.imgctx.fillStyle = '#fff';
        self.imgctx.strokeStyle = self.color_2;
        self.imgctx.arc(eyeX, eyeY, eyeRadius, 0, Math.PI * 2);
        self.imgctx.fill();
        self.imgctx.stroke();

        self.imgctx.beginPath();
        self.imgctx.fillStyle = '#000';
        self.imgctx.arc(eyeX + eyeRadius / 2, eyeY, eyeRadius / 4, 0, Math.PI * 2);
        self.imgctx.fill();
      }

      // 画左眼
      drawEye(this.img.width / 2 + this.paintWidth / 2 - eyeRadius, this.img.height / 2 - this.paintHeight / 2 + eyeRadius);

      // 画右眼
      drawEye(this.img.width / 2 + this.paintWidth / 2 - eyeRadius, this.img.height / 2 + this.paintHeight / 2 - eyeRadius);
    }

    /**
     * 转向某个角度
     */

  }, {
    key: 'directTo',
    value: function directTo(angle) {
      // 老的目标角度, 但是是小于360度的, 因为每次计算出来的目标角度也是0 - 360度
      var oldAngle = Math.abs(this.toAngle % (Math.PI * 2));

      // 转了多少圈
      var rounds = ~ ~(this.toAngle / (Math.PI * 2));

      this.toAngle = angle;

      if (oldAngle >= Math.PI * 3 / 2 && this.toAngle <= Math.PI / 2) {
        // 角度从第四象限左划至第一象限, 增加圈数
        rounds++;
      } else if (oldAngle <= Math.PI / 2 && this.toAngle >= Math.PI * 3 / 2) {
        // 角度从第一象限划至第四象限, 减少圈数
        rounds--;
      }

      // 计算真实要转到的角度
      this.toAngle += rounds * Math.PI * 2;
    }

    // 根据蛇头角度计算水平速度和垂直速度

  }, {
    key: 'velocity',
    value: function velocity() {
      var angle = this.angle % (Math.PI * 2);
      var vx = Math.abs(this.speed * Math.sin(angle));
      var vy = Math.abs(this.speed * Math.cos(angle));

      if (angle < Math.PI / 2) {
        this.vx = vx;
        this.vy = -vy;
      } else if (angle < Math.PI) {
        this.vx = vx;
        this.vy = vy;
      } else if (angle < Math.PI * 3 / 2) {
        this.vx = -vx;
        this.vy = vy;
      } else {
        this.vx = -vx;
        this.vy = -vy;
      }
    }

    /**
     * 增加蛇头的逐帧逻辑
     */

  }, {
    key: 'update',
    value: function update() {
      this.turnAround();

      this.velocity();

      _get(Object.getPrototypeOf(SnakeHeader.prototype), 'update', this).call(this);

      // 不让蛇走出边界
      var whalf = this.width / 2;
      if (this.x < whalf) {
        this.x = whalf;
      } else if (this.x + whalf > _map2.default.width) {
        this.x = _map2.default.width - whalf;
      }

      var hhalf = this.height / 2;
      if (this.y < hhalf) {
        this.y = hhalf;
      } else if (this.y + hhalf > _map2.default.height) {
        this.y = _map2.default.height - hhalf;
      }
    }

    /**
     * 蛇头转头
     */

  }, {
    key: 'turnAround',
    value: function turnAround() {
      var angleDistance = this.toAngle - this.angle; // 与目标角度之间的角度差
      var turnSpeed = 0.045; // 转头速度

      // 当转到目标角度, 重置蛇头角度
      if (Math.abs(angleDistance) <= turnSpeed) {
        this.toAngle = this.angle = BASE_ANGLE + this.toAngle % (Math.PI * 2);
      } else {
        this.angle += Math.sign(angleDistance) * turnSpeed;
      }
    }
  }]);

  return SnakeHeader;
}(SnakeBase);

// 蛇类


var Snake = function () {
  function Snake(options) {
    _classCallCheck(this, Snake);

    this.bodys = [];
    this.point = 0;

    // 创建脑袋
    this.header = new SnakeHeader(options);

    // 创建身躯, 给予各个身躯跟踪目标
    options.tracer = this.header;
    for (var i = 0; i < options.length; i++) {
      this.bodys.push(options.tracer = new SnakeBody(options));
    }

    this.binding();
  }

  _createClass(Snake, [{
    key: 'binding',


    /**
     * 蛇与鼠标的交互事件
     */
    value: function binding() {
      var self = this;

      if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {
        window.addEventListener('touchmove', function (e) {
          e.preventDefault();
          self.moveTo(e.touches[0].pageX, e.touches[0].pageY);
        });

        window.addEventListener('touchstart', function (e) {
          e.preventDefault();
          self.moveTo(e.touches[0].pageX, e.touches[0].pageY);
        });
      } else {
        // 蛇头跟随鼠标的移动而变更移动方向
        window.addEventListener('mousemove', function () {
          var e = arguments.length <= 0 || arguments[0] === undefined ? window.event : arguments[0];
          return self.moveTo(e.clientX, e.clientY);
        });

        // 鼠标按下让蛇加速
        window.addEventListener('mousedown', self.speedUp.bind(self));

        // 鼠标抬起停止加速
        window.addEventListener('mouseup', self.speedDown.bind(self));
      }
    }

    /**
     * 加速
     */

  }, {
    key: 'speedUp',
    value: function speedUp() {
      this.header.speed = 5;
      this.bodys.forEach(function (body) {
        body.speed = 5;
      });
    }

    /**
     * 恢复原速度
     */

  }, {
    key: 'speedDown',
    value: function speedDown() {
      this.header.speed = SPEED;
      this.bodys.forEach(function (body) {
        body.speed = SPEED;
      });
    }

    /**
     * 根据传入坐标, 获取坐标点相对于蛇的角度
     * @param x
     * @param y
     */

  }, {
    key: 'moveTo',
    value: function moveTo(nx, ny) {
      var x = nx - this.header.paintX;
      var y = this.header.paintY - ny;
      var angle = Math.atan(Math.abs(x / y));

      // 计算角度, 角度值为 0-360
      if (x > 0 && y < 0) {
        angle = Math.PI - angle;
      } else if (x < 0 && y < 0) {
        angle = Math.PI + angle;
      } else if (x < 0 && y > 0) {
        angle = Math.PI * 2 - angle;
      }

      this.header.directTo(angle);
    }

    /**
     * 吃掉食物
     * @param food
     */

  }, {
    key: 'eat',
    value: function eat(food) {
      this.foodNum = this.foodNum || 0;
      this.point += food.point;
      this.foodNum++;

      // 增加分数引起虫子体积增大
      var added = food.point / 100;
      var newSize = this.header.width + added;
      this.header.setSize(newSize);
      this.bodys.forEach(function (body) {
        body.setSize(newSize);
      });

      // 调整地图缩放比例
      _map2.default.setScale(_map2.default.scale + added / (this.header.width * 3));

      // 每吃两个个食物, 都增加一截身躯
      if (this.foodNum % 2 === 0) {
        var lastBody = this.bodys[this.bodys.length - 1];
        this.bodys.push(new SnakeBody({
          x: lastBody.x,
          y: lastBody.y,
          size: lastBody.width,
          color: lastBody.color,
          tracer: lastBody
        }));
      }
    }

    // 渲染蛇头蛇身

  }, {
    key: 'render',
    value: function render() {
      for (var i = this.bodys.length - 1; i >= 0; i--) {
        this.bodys[i].render();
      }

      this.header.render();
    }
  }, {
    key: 'x',
    get: function get() {
      return this.header.x;
    }
  }, {
    key: 'y',
    get: function get() {
      return this.header.y;
    }
  }]);

  return Snake;
}();

exports.default = Snake;

/*****************
 ** WEBPACK FOOTER
 ** ./src/lib/snake.js
 ** module id = 3
 ** module chunks = 0
 **/