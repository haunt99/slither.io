'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Created by wanghx on 4/27/16.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * base    地图上的元素基础类, 默认为圆
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * 基础属性:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - x             元素的中心点x坐标
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - y             元素的中心店y坐标
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - width         元素宽度
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - height        元素高度
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - paintX        元素的绘制坐标
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - paintY        元素的绘制坐标
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - paintWidth    元素的绘制宽度
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - paintHeight   元素的绘制高度
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *  - visible       元素是否在视窗内可见
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */

var _map = __webpack_require__(5);

var _map2 = _interopRequireDefault(_map);

var _frame = __webpack_require__(6);

var _frame2 = _interopRequireDefault(_frame);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Base = function () {
  function Base(options) {
    _classCallCheck(this, Base);

    this.x = options.x;
    this.y = options.y;
    this.width = options.size || options.width;
    this.height = options.size || options.height;
  }

  /**
   * 绘制时的x坐标, 要根据视窗来计算位置
   * @returns {number}
   */


  _createClass(Base, [{
    key: 'paintX',
    get: function get() {
      return _map2.default.relative(this.x) - _frame2.default.x;
    }

    /**
     * 绘制时的y坐标, 要根据视窗来计算位置
     * @returns {number}
     */

  }, {
    key: 'paintY',
    get: function get() {
      return _map2.default.relative(this.y) - _frame2.default.y;
    }

    /**
     * 绘制宽度
     * @returns {*}
     */

  }, {
    key: 'paintWidth',
    get: function get() {
      return _map2.default.relative(this.width);
    }

    /**
     * 绘制高度
     * @returns {*}
     */

  }, {
    key: 'paintHeight',
    get: function get() {
      return _map2.default.relative(this.height);
    }

    /**
     * 在视窗内是否可见
     * @returns {boolean}
     */

  }, {
    key: 'visible',
    get: function get() {
      var paintX = this.paintX;
      var paintY = this.paintY;
      var halfWidth = this.paintWidth / 2;
      var halfHeight = this.paintHeight / 2;

      return paintX + halfWidth > 0 && paintX - halfWidth < _frame2.default.width && paintY + halfHeight > 0 && paintY - halfHeight < _frame2.default.height;
    }
  }]);

  return Base;
}();

exports.default = Base;

/*****************
 ** WEBPACK FOOTER
 ** ./src/lib/base.js
 ** module id = 4
 ** module chunks = 0
 **/