'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _base = __webpack_require__(4);

var _base2 = _interopRequireDefault(_base);

var _map = __webpack_require__(5);

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by wanghx on 4/27/16.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * food
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var Food = function (_Base) {
  _inherits(Food, _Base);

  function Food(options) {
    _classCallCheck(this, Food);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Food).call(this, options));

    _this.point = options.point;
    _this.lightSize = _this.width / 2; // 食物的半径, 发光半径
    _this.lightDirection = true; // 发光动画方向
    return _this;
  }

  _createClass(Food, [{
    key: 'update',
    value: function update() {
      var lightSpeed = 1;

      this.lightSize += this.lightDirection ? lightSpeed : -lightSpeed;

      // 当发光圈到达一定值再缩小
      if (this.lightSize > this.width || this.lightSize < this.width / 2) {
        this.lightDirection = !this.lightDirection;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      if (!this.visible) {
        return;
      }

      this.update();

      _map2.default.ctx.fillStyle = '#fff';

      // 绘制光圈
      _map2.default.ctx.globalAlpha = 0.2;
      _map2.default.ctx.beginPath();
      _map2.default.ctx.arc(this.paintX, this.paintY, this.lightSize * this.paintWidth / this.width, 0, Math.PI * 2);
      _map2.default.ctx.fill();

      // 绘制实体
      _map2.default.ctx.globalAlpha = 1;
      _map2.default.ctx.beginPath();
      _map2.default.ctx.arc(this.paintX, this.paintY, this.paintWidth / 2, 0, Math.PI * 2);
      _map2.default.ctx.fill();
    }
  }]);

  return Food;
}(_base2.default);

exports.default = Food;

/*****************
 ** WEBPACK FOOTER
 ** ./src/lib/food.js
 ** module id = 8
 ** module chunks = 0
 **/