'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _frame = __webpack_require__(6);

var _frame2 = _interopRequireDefault(_frame);

var _eventemitter = __webpack_require__(7);

var _eventemitter2 = _interopRequireDefault(_eventemitter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by wanghx on 4/25/16.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * 地图类 由于地图在整个游戏中只有一个, 所以做成单例
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

// 地图类

var Map = function (_EventEmitter) {
  _inherits(Map, _EventEmitter);

  function Map() {
    _classCallCheck(this, Map);

    // 背景块的大小

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Map).call(this));

    _this.blockWidth = 150;
    _this.blockHeight = 150;
    return _this;
  }

  /**
   * 初始化map对象
   * @param options
   */


  _createClass(Map, [{
    key: 'init',
    value: function init(options) {
      this.canvas = options.canvas;
      this.ctx = this.canvas.getContext('2d');

      // 地图大小
      this.width = options.width;
      this.height = options.height;

      // 地图比例
      this.scale = options.scale || 1;
    }

    /**
     * 设置缩放比例
     * @param value
     */

  }, {
    key: 'setScale',
    value: function setScale(scale) {
      this.toScale = scale;
    }

    /**
     * 清空地图上的内容
     */

  }, {
    key: 'clear',
    value: function clear() {
      this.ctx.clearRect(0, 0, _frame2.default.width, _frame2.default.height);
    }

    /**
     * 相对于地图scale的值
     * @param val
     * @returns {*}
     */

  }, {
    key: 'relative',
    value: function relative(val) {
      return val + val * (1 - this.scale);
    }

    /**
     * 对地图本身的更新都在此处进行
     */

  }, {
    key: 'update',
    value: function update() {
      if (this.toScale && this.scale !== this.toScale) {
        this.scale = this.toScale;
      }
    }

    /**
     * 渲染地图
     */

  }, {
    key: 'render',
    value: function render() {
      this.ctx.save();
      var beginX = _frame2.default.x < 0 ? -_frame2.default.x : -_frame2.default.x % this.paintBlockWidth;
      var beginY = _frame2.default.y < 0 ? -_frame2.default.y : -_frame2.default.y % this.paintBlockHeight;
      var endX = _frame2.default.x + _frame2.default.width > this.paintWidth ? this.paintWidth - _frame2.default.x : beginX + _frame2.default.width + this.paintBlockWidth;
      var endY = _frame2.default.y + _frame2.default.height > this.paintHeight ? this.paintHeight - _frame2.default.y : beginY + _frame2.default.height + this.paintBlockHeight;

      // 铺底色
      this.ctx.fillStyle = '#999';
      this.ctx.fillRect(beginX, beginY, endX - beginX, endY - beginY);

      // 画方格砖
      this.ctx.lineWidth = 0.5;
      this.ctx.strokeStyle = '#fff';
      for (var x = beginX; x <= endX; x += this.paintBlockWidth) {
        for (var y = beginY; y <= endY; y += this.paintBlockWidth) {
          var cx = endX - x;
          var cy = endY - y;
          var w = cx < this.paintBlockWidth ? cx : this.paintBlockWidth;
          var h = cy < this.paintBlockHeight ? cy : this.paintBlockHeight;

          this.ctx.strokeRect(x, y, w, h);
        }
      }

      this.ctx.restore();
    }

    /**
     * 画小地图
     */

  }, {
    key: 'renderSmallMap',
    value: function renderSmallMap() {
      // 小地图外壳, 圆圈
      var margin = 30;
      var smapr = 50;
      var smapx = _frame2.default.width - smapr - margin;
      var smapy = _frame2.default.height - smapr - margin;

      // 地图在小地图中的位置和大小
      var smrect = 50;
      var smrectw = this.paintWidth > this.paintHeight ? smrect : this.paintWidth * smrect / this.paintHeight;
      var smrecth = this.paintWidth > this.paintHeight ? this.paintHeight * smrect / this.paintWidth : smrect;
      var smrectx = smapx - smrectw / 2;
      var smrecty = smapy - smrecth / 2;

      // 相对比例
      var radio = smrectw / this.paintWidth;

      // 视窗在小地图中的位置和大小
      var smframex = _frame2.default.x * radio + smrectx;
      var smframey = _frame2.default.y * radio + smrecty;
      var smframew = _frame2.default.width * radio;
      var smframeh = _frame2.default.height * radio;

      this.ctx.save();
      this.ctx.globalAlpha = 0.8;

      // 画个圈先
      this.ctx.beginPath();
      this.ctx.arc(smapx, smapy, smapr, 0, Math.PI * 2);
      this.ctx.fillStyle = '#000';
      this.ctx.fill();
      this.ctx.stroke();

      // 画缩小版地图
      this.ctx.fillStyle = '#999';
      this.ctx.fillRect(smrectx, smrecty, smrectw, smrecth);

      // 画视窗
      //    this.ctx.strokeRect(smframex, smframey, smframew, smframeh);

      this.ctx.restore();

      // 画蛇蛇位置
      this.ctx.fillStyle = '#fff';
      this.ctx.fillRect(smframex + smframew / 2 - 2, smframey + smframeh / 2 - 2, 4, 4);
    }
  }, {
    key: 'scale',
    set: function set(value) {
      if (value >= 1.6 || value < 1 || this._scale === value) {
        return;
      }

      this._scale = value;

      this.paintBlockWidth = this.relative(this.blockWidth);
      this.paintBlockHeight = this.relative(this.blockHeight);
      this.paintWidth = this.relative(this.width);
      this.paintHeight = this.relative(this.height);

      this.emit('scale_changed');
    },
    get: function get() {
      return this._scale;
    }
  }]);

  return Map;
}(_eventemitter2.default);

exports.default = new Map();

/*****************
 ** WEBPACK FOOTER
 ** ./src/lib/map.js
 ** module id = 5
 ** module chunks = 0
 **/