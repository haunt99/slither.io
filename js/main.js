'use strict';

var _stats = __webpack_require__(2);

var _stats2 = _interopRequireDefault(_stats);

var _snake = __webpack_require__(3);

var _snake2 = _interopRequireDefault(_snake);

var _food = __webpack_require__(8);

var _food2 = _interopRequireDefault(_food);

var _frame = __webpack_require__(6);

var _frame2 = _interopRequireDefault(_frame);

var _map = __webpack_require__(5);

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var raf = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
  window.setTimeout(callback, 1000 / 60);
}; /**
    * Created by wanghx on 4/23/16.
    *
    * main
    *
    */

var canvas = document.getElementById('cas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// fps状态
var stats = new _stats2.default();
document.body.appendChild(stats.dom);

// 初始化地图对象
_map2.default.init({
  canvas: canvas,
  width: 10000,
  height: 10000
});

// 初始化视窗对象
_frame2.default.init({
  x: 1000,
  y: 1000,
  width: canvas.width,
  height: canvas.height
});

// 创建蛇类对象
var snake = new _snake2.default({
  x: _frame2.default.x + _frame2.default.width / 2,
  y: _frame2.default.y + _frame2.default.height / 2,
  size: 40,
  length: 10,
  color: '#fff'
});

// 食物生成方法
var foodsNum = 1000;
var foods = [];
function createFood(num) {
  for (var i = 0; i < num; i++) {
    var point = ~ ~(Math.random() * 30 + 50);
    var size = ~ ~(point / 3);

    foods.push(new _food2.default({
      x: ~ ~(Math.random() * (_map2.default.width - 2 * size) + size),
      y: ~ ~(Math.random() * (_map2.default.height - 2 * size) + size),
      size: size, point: point
    }));
  }
}

/**
 * 碰撞检测
 * @param dom
 * @param dom2
 * @param isRect   是否为矩形
 */
function collision(dom, dom2, isRect) {
  var disX = dom.x - dom2.x;
  var disY = dom.y - dom2.y;

  if (isRect) {
    return Math.abs(disX) < dom.width + dom2.width && Math.abs(disY) < dom.height + dom2.height;
  }

  return Math.hypot(disX, disY) < (dom.width + dom2.width) / 2;
}

// 创建一些食物
createFood(foodsNum);

// 动画逻辑
var timeout = 0;
var time = new Date();
function animate() {
  var ntime = new Date();

  stats.begin();

  if (ntime - time > timeout) {
    _map2.default.clear();

    // 让视窗跟随蛇的位置更改而更改
    _frame2.default.track(snake);

    _map2.default.render();

    // 渲染食物, 以及检测食物与蛇头的碰撞
    foods.slice(0).forEach(function (food) {
      food.render();

      if (food.visible && collision(snake.header, food)) {
        foods.splice(foods.indexOf(food), 1);
        snake.eat(food);
        createFood(1);
      }
    });

    snake.render();

    _map2.default.renderSmallMap();

    _map2.default.update();

    time = ntime;
  }

  stats.end();

  raf(animate);
}

animate();

/*****************
 ** WEBPACK FOOTER
 ** ./src/main.js
 ** module id = 1
 ** module chunks = 0
 **/